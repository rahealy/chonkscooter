**The Chonk!**

The Chonk! is a recumbent scooter/cycle with wide wheels and negative trail steering based on the python-lowracer. The Chonk's design goals include adjustable dimensions to accommodate passengers up to 2 meters tall and weights up to 180 kg.

This project is intended as an educational endeavor to better learn about CAD, physics, engineering, and modeling physical systems.

**A note from the original creator (Richard Healy)**

As an educational endeavor The Chonk does not concern itself with with traditional designs that appeal to any particular demographic. Instead, The Chonk concerns itself with comfort, style, and performance. 

Two wheeled enthusiasts, in general, seem to prefer traditional designs. While _I_ think The Chonk looks cool and I'd be delighted if others think it looks cool too, what's really important to me is that _everyone's_ preference and sense of two wheeled style be tolerated and hopefully encouraged on the road.
