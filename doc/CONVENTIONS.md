**Conventions**

This document defines some general conventions that are more or less followed throughout the drawings.

**Names And Labels**

When possible FreeCAD object names and labels use the camel case (CamelCase) naming convention. The following FreeCAD objects are not renamed:

* `Origin`

**Orientations**

Orientations of the Chonk are relative to the scooter in normal operation outside with wheels touching the ground. Follows are the orientations in order of precedence:

Bottom - Toward the ground.
Top - Toward the sky.
Left - Left side facing toward the front.
Right - Right side facing toward the front.
Front - Direction of forward motion in normal operation.
Rear - Direction opposite of forward motion in normal operation.

**Pipes**

FreeCAD pipes require two objects. Follows are the objects in their order of precedence:

Profile - Object that will define the outside profile of the pipe.
Path - Object that will describe the path taken by the profile to make the pipe.

**Part**

Files and FreeCAD groups containing "part" in their names or labels contain individual parts and part assemblies for copying and pasting into the assembly.

**Assembly**

Files and FreeCAD groups containing "assembly" in their names or labels contain edited copies of parts and are assembled together.

**Reference**

FreeCAD objects containing "reference" in their names or labels contain objects and spreadsheets referenced by children of the Reference object's parent.

**Reference Coordinate Systems (CS)**

FreeCAD reference objects contain one or more coordinate systems that reference sketches refer to. Coordinate system names or labels end with the "CS" suffix.

**Coordinate Systems (CS)**

FreeCAD groups contain one or more coordinate systems that sketches in the group refer to. The coordinate systems may be based on a reference sketch through a binder.

When possible, coordinate system X,Y, and Z axes are oriented to be parallel to the FreeCAD root origin. 

**Binders**

FreeCAD groups use FreeCAD binder objects to reference sketches in different groups. Binder names and labels use the name of the bound object followed by the word Binder.

**Spreadsheets**

FreeCAD spreadsheets along with spreadsheet aliases are used to hold dimensional values and their names and descriptions.

**Aliases**

FreeCAD spreadsheet cell aliases always begin with a lower case letter and follow the camel case (camelCase) naming convention.
