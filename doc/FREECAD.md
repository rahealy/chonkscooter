**FreeCAD**

Like any piece of complicated software FreeCAD has certain ways of doing things. This document provides information on what was tried and what was found to work.

**Parts and Groups**

In software there's a term called "scope" which refers to what parts of software can access other parts of software. In the FreeCAD GUI and drawings scopes seems to be as follows:

* Spreadsheets can be accessed by name from anywhere.
* Groups can access sketches in the same group. Use Shape Binders to access sketches outside of the group.
* Parts contain groups.

**Setting and Getting Via Python**

After experimenting with several ways of setting values in the FreeCAD drawing it was found that using named constraints seemed to work best for animating.

Setting and getting is not consistent throughout the FreeCAD API. Research best methods for different objects.

Setting aliased spreadsheet cells did not update the drawing features using the value in the cell despite updating both FreeCAD spreadsheet and drawing objects.

**Selecting Items**

Item selection in sketches and drawings is sensitive to off plane orientations. Sometimes better results are had by flipping over the sketch and working from the opposite side.

**InertialCS Warnings**

If FreeCAD is invoked from a shell the following warning may appear when a drawing is opened or refreshed:

```
AttachEngine3D::calculateAttachedPlacement:InertialCS: inertia tensor has axis of symmetry. Second and third axes of inertia are undefined.
```

These warnings slow down refreshing considerably. To eliminate them find all sketches with a map mode of 'InertialCS' and pick another map mode. To assist in identifying which sketches need altered use the script below in the GUI python console (**View->Panels->Python console**). 

```
for obj in App.ActiveDocument.Objects:
    if obj.Name.startswith('Sketch'):
        if obj.MapMode == 'InertialCS':
            print(obj.Label)
```


**Drawing Articulated Items**

Articulation is accomplished via FreeCAD object Attachment properties. Attach the child object to three vertices in the parent object (origin, horizontal plane, vertical plane.), set Align mode to OXY, adjust "AttachmentOffset" parameters via Python.

```
>>> #Adjust position
>>> sktch.AttachmentOffset.Base
Vector (11.0, 0.0, 0.0)
>>> sktch.AttachmentOffset.Base.x = 0
>>> sktch.AttachmentOffset.Base
Vector (0.0, 0.0, 0.0)
>>>
>>> #Adjust rotation
>>> sktch.AttachmentOffset.Rotation
Rotation (-0.9, 0.0, -0.0, 0.2)
>>> sktch.AttachmentOffset.Rotation = FreeCAD.Base.Rotation(-0.9, 0.0, -0.0, 0.2)
```
