# -*- coding: utf-8 -*-
#-------------------------------------------------
# Copyright Richard A. Healy (https://gitlab.com/rahealy)
#
# This work is licensed under the Creative Commons Attribution-ShareAlike
# 4.0 International License. To view a copy of this license, visit 
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to 
# Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
#-------------------------------------------------
#
#-------------------------------------------------
# CHONK! The chonky scooter simulator.
#
# Command Line:
#  $ cd /path/to/chonkscooter/cad/
#  $ freecad simulation.py &
#-------------------------------------------------
#

import math
import FreeCAD
from PySide import QtCore, QtGui

#-------------------------------------------------
# Functions
#-------------------------------------------------

def lineout(strval):
	"""Print string to console and append newline."""
	App.Console.PrintMessage('[simulation] ' + strval + '\n')


#-------------------------------------------------
# Classes
#-------------------------------------------------

#-------------------------------------------------
# Updater()
#-------------------------------------------------

class Updater(object):
	"""Defines an interface for an object that maintains update state."""
	def __init__(self, updated):
		self.updated = updated
	def update_from(self, updater):
		if updater.updated:
			self.update(updater.value())
	def update(self, val):
		pass
	def value(self):
		pass
	def reset(self):
		self.updated = False


class VariableUpdater(Updater):
	"""Updates a generic variable."""
	def __init__(self, var):
		Updater.__init__(self, False)
		self.variable = var
		self.updated = False
	def update(self, var):
		if self.variable != var:
			self.variable = var
			self.updated = True
			return var
		return None
	def value(self):
		return self.variable


class ConstraintUpdater(Updater):
	"""Updates a named constraint in a FreeCAD sketch."""
	def __init__(self):
		Updater.__init__(self, False)
		self.sketch = None
		self.conidx = None
		self.suffix = ' deg'

	def update(self, var):
		cur = self.sketch.getDatum(self.conidx).Value
		if cur != var:
			new = App.Units.Quantity(str(var) + ' deg')
			self.sketch.setDatum(self.conidx, new)
			self.sketch.recompute()
			self.updated = True

	def value(self):
		return self.sketch.getDatum(self.conidx)

	def setSketchAndConstraint(self, doc, sketchlbl, conname):
		"""
		Set sketch and named constraint to update.
		"""
		lineout (
			'ConstraintUpdater.setSketchAndConstraint(): ' + 
			'Searching for sketch ' + sketchlbl + '...'
		)
		objs = doc.getObjectsByLabel(sketchlbl)
		if objs:
			lineout (
				'ConstraintUpdater.setSketchAndConstraint(): ' + 
				'Found sketch ' + sketchlbl + '.'
			)
			self.sketch = objs[0]
			lineout (
				'ConstraintUpdater.setSketchAndConstraint(): ' + 
				'Searching for constraint ' + conname + '...'
			)
			i = 0
			for con in self.sketch.Constraints:
				if con.Name == conname:
					self.conidx = i
					lineout(
						'ConstraintUpdater.setSketchAndConstraint(): ' +
						'Found constraint at ' + str(i) + '.'
					)
					return None
				i += 1
			lineout('ConstraintUpdater.setSketchAndConstraint(): Constraint not found.')
			self.conidx = None
		else:
			lineout('ConstraintUpdater.setSketchAndConstraint(): Sketch not found.')


class SheetUpdater(Updater):
	"""Updates an aliased FreeCAD spreadsheet cell."""
	def __init__(self):
		Updater.__init__(self, False)
		self.sheet = None
		self.alias = None
		
	def update(self, var):
		if self.sheet.get(self.alias) != var:
			self.sheet.set(self.alias, str(var))
			self.sheet.recompute()
			self.updated = True
			return var
		return None
	
	def value(self):
		return getattr(self.sheet, self.alias)
	
	def setSpreadsheetAndAlias(self, doc, sheetlbl, alias):
		"""
		Set spreadsheet and alias to update in document.
		"""
		lineout ('SheetAliasUpdater.setSpreadsheetAndAlias(): Searching for sheet ' + sheetlbl + '...')
		objs = doc.getObjectsByLabel(sheetlbl)
		if objs:
			lineout('SheetAliasUpdater.setSpreadsheetAndAlias(): Found sheet ' + sheetlbl + '.')
			self.sheet = objs[0]
			lineout ('SheetAliasUpdater.setSpreadsheetAndAlias(): Searching for alias ' + alias + '...')
			if hasattr(self.sheet, alias):
				self.alias = alias
				return None
			else:
				lineout('SheetAliasUpdater.setSpreadsheetAndAlias(): Alias not found.')
		else:
			lineout('SheetAliasUpdater.setSpreadsheetAndAlias(): Sheet not found.')


#-------------------------------------------------
# Model()
#-------------------------------------------------

class BumpModel(object):
	"""Models a bump in the road using a b-spline defined in the FreeCAD assembly drawing."""
	def __init__(self):
		return


class SpringModel(object):
	"""Models a dampened spring."""
	def __init__(self):
		return
	
	def force(compression):
		"""Return the force needed to compress the spring by compression percent."""
		return


#-------------------------------------------------
# Chonk()
#-------------------------------------------------

class ChonkUpdater(object):
	"""
	Provides an interface for storing and updating variables common to
	the simulation model and view.
	
	Members:
		rearSwingArmAngle - rear swing arm angle in degrees.
		frontSwingArmAngle - front swing arm angle in degrees.
		elevationPos - elevation in mm. A value of 0 indicates static suspension load, flat ground.
		pitchAngle - Pitch of frame relative to flat ground.
	"""
	def __init__ (
		self,
		rearSwingArmAngle,
		frontSwingArmAngle,
		elevationPos,
		pitchAngle
	):
		self.rearSwingArmAngle = rearSwingArmAngle 
		self.frontSwingArmAngle = frontSwingArmAngle
		self.elevationPos = elevationPos
		self.pitchAngle = pitchAngle

	def update(self, frm):
		"""Updates all the values from the provided ChonkUpdater() implementation."""
		self.rearSwingArmAngle.update(frm.rearSwingArmAngle.value())
		self.frontSwingArmAngle.update(frm.frontSwingArmAngle.value())
		self.elevationPos.update(frm.elevationPos.value())
		self.pitchAngle.update(frm.pitchAngle.value())

	def update_from(self, frm):
		"""Updates all the updated values from the provided ChonkUpdater() implementation."""
		self.rearSwingArmAngle.update_from(frm.rearSwingArmAngle)
		self.frontSwingArmAngle.update_from(frm.frontSwingArmAngle)
		self.elevationPos.update_from(frm.elevationPos)
		self.pitchAngle.update_from(frm.pitchAngle)

	def updated(self):
		"""Returns true if any of the ChonkUpdater() members have been updated."""
		return (
			self.rearSwingArmAngle.updated | 
			self.frontSwingArmAngle.updated |
			self.elevationPos.updated |
			self.pitchAngle.updated
		)

	def reset(self):
		self.rearSwingArmAngle.reset()
		self.frontSwingArmAngle.reset()
		self.elevationPos.reset()
		self.pitchAngle.reset()


class ChonkModel(ChonkUpdater):
	"""
	Provides a physics based model of the Chonk scooter's parameters.
	
	Inherits:
		ChonkUpdater()
		
	Members:
		chonkView - Reference to view which is updated when model changed. 
		rearSpringModel - Models the rear swingarm spring.
		frontSpringModel - Models the front swingarm spring.
		bumpModel - Models a bump.
	"""
	def __init__(self, view):
		ChonkUpdater.__init__ (
			self, 
			rearSwingArmAngle = VariableUpdater(0.0),
			frontSwingArmAngle = VariableUpdater(0.0), 
			elevationPos = VariableUpdater(0.0), 
			pitchAngle = VariableUpdater(0.0),
		)
		self.chonkView = view
		self.rearSpringModel = SpringModel()
		self.frontSpringModel = SpringModel()
		self.bumpModel = BumpModel()

	def recompute(self):
		if self.updated():
			self.chonkView.update_from(self)
			self.chonkView.recompute()
			self.reset()


class ChonkView(ChonkUpdater):
	"""
	Provides an interface to the FreeCAD assembly drawing.
	
	Inherits:
		ChonkUpdater()
		
	Members:
		cadDocument - FreeCAD assembly drawing document object.
	"""
	def __init__(self):
		"""Initialize the view."""
		ChonkUpdater.__init__ (
			self, 
			rearSwingArmAngle = ConstraintUpdater(),
			frontSwingArmAngle = ConstraintUpdater(),
			elevationPos =  VariableUpdater(0.0),
			pitchAngle = VariableUpdater(0.0),
		)
		self.cadDocument = None

	def openAssemblyDrawing(self, fpath):
		"""
		Open assembly drawing in FreeCAD and get references to constraints.
		
		Arguments:
			fpath - Path to "assembly.FCStd"
		
		Returns:
			None if document opened otherwise undefined.
		"""
		fname = fpath + 'assembly.FCStd'
		try:
			lineout('ChonkView.openAssembly(): Opening assembly drawing ' + fname + '...')
			self.cadDocument = FreeCAD.open(fname)
		except:
			lineout('ChonkView.openAssembly(): failed to open assembly drawing.')
			return
		else:
			lineout('ChonkView.openAssembly(): Opened assembly drawing.')
			self.rearSwingArmAngle.setSketchAndConstraint (
				self.cadDocument,
				'RearSwingArmPositionReferenceSketch',
				'swingArmAngle'
			)
			self.frontSwingArmAngle.setSketchAndConstraint (
				self.cadDocument,
				'FrontSwingArmPositionReferenceSketch',
				'swingArmAngle'
			)
			return None

	def recompute(self):
		"""Recompute (redraw) the drawing and reset parameter states to unchanged"""
		self.cadDocument.recompute()
		self.reset()


class ChonkController(object):
	"""
	Takes input from Qt GUI to control the simulation model.
	
	Members:
		chonkModel - ChonkModel() object affected by controller input.
		guiDialog - Qt dialog object generated by loadGUI.
		qtTimer - Cyclic timer keeps track of motion frames.
	"""
	def __init__(self, model):
		"""
		Initialize the controller.
		
		Arguments:
			model_obj - Model to update when timer elapses.
		"""
		self.chonkModel = model
		self.guiDialog = None
		self.qtTimer = QtCore.QTimer()
		QtCore.QObject.connect (
			self.qtTimer,
			QtCore.SIGNAL("timeout()"), 
			self.onTimeout
		)
		return

	def startTimer(self, msec):
		"""
		Start the repeating refresh timer. 
		
		Arguments:
			msec - Delay in milliseconds between refreshes.
		"""
		self.qtTimer.start(msec)

	def loadGUI(self, fpath):
		"""
		Load the simulator's Qt user interface.
		
		Arguments:
			fpath - Path to Qt "dialog.ui" file.
		
		Returns:
			None if document opened. Undefined otherwise.
		"""
		fname = fpath + 'dialog.ui'
		try:
			lineout('ChonkController.loadUI(): Loading UI file ' + fname + '...')
			self.guiDialog = FreeCADGui.PySideUic.loadUi(fname)
		except:
			lineout('ChonkController.loadUI(): failed to open user interface file.')
			return
		else:
			self.guiDialog.show();
			lineout('ChonkController.loadUI(): Loaded.')
			self.guiDialog.frontSwingArmAngleSlider.valueChanged[int].connect (
				self.onFrontSwingArmAngleSliderChanged
			)
			self.guiDialog.rearSwingArmAngleSlider.valueChanged[int].connect (
				self.onRearSwingArmAngleSliderChanged
			)
			self.guiDialog.frontSwingArmAngleSpinBox.valueChanged[float].connect (
				self.onFrontSwingArmAngleSpinBoxChanged
			)
			self.guiDialog.rearSwingArmAngleSpinBox.valueChanged[float].connect (
				self.onRearSwingArmAngleSpinBoxChanged
			)
			self.guiDialog.recomputeButton.clicked.connect (
				self.onRecomputeButtonClicked
			)
			return None

	def update(self, updater):
		"""Use ChonkUpdater() to update GUI controls."""
		self.guiDialog.frontSwingArmAngleSlider.setValue(
			updater.frontSwingArmAngle.value()
		)
		self.guiDialog.rearSwingArmAngleSlider.setValue(
			updater.rearSwingArmAngle.value()
		)

	def onFrontSwingArmAngleSliderChanged(self, val):
		"""On slider change update spinbox value."""
		self.guiDialog.frontSwingArmAngleSpinBox.setValue(val)
		self.chonkModel.frontSwingArmAngle.update(val)

	def onRearSwingArmAngleSliderChanged(self, val):
		"""On slider change update spinbox value."""
		self.guiDialog.rearSwingArmAngleSpinBox.setValue(val)
		self.chonkModel.rearSwingArmAngle.update(val)

	def onFrontSwingArmAngleSpinBoxChanged(self, val):
		"""On spinbox change update slider value."""
		self.guiDialog.frontSwingArmAngleSlider.setValue(val)
		self.chonkModel.frontSwingArmAngle.update(val)

	def onRearSwingArmAngleSpinBoxChanged(self, val):
		"""On spinbox change update slider value."""
		self.guiDialog.rearSwingArmAngleSlider.setValue(val)
		self.chonkModel.rearSwingArmAngle.update(val)

	def onTimeout(self):
		"""Timer tick has elapsed. Update model to next frame."""
		self.chonkModel.recompute()

	def onRecomputeButtonClicked(self):
		"""Timer tick has elapsed. Update model to next frame."""
		self.chonkModel.recompute()


#-------------------------------------------------
# Main
#-------------------------------------------------

view = ChonkView()
model = ChonkModel(view)
cntrlr = ChonkController(model)

#Open the drawing.
view.openAssemblyDrawing('')

#Update the model parameters with the drawing values in the view.
model.update(view)

#Load the GUI.
cntrlr.loadGUI('')

#Update the controller parameters with the values in the model.
cntrlr.update(model)

#Errata
	#App.setActiveDocument("assembly")
	#App.ActiveDocument=App.getDocument("assembly")
	#Gui.ActiveDocument=Gui.getDocument("assembly")
