**The Chonk!**

The Chonk! is a recumbent scooter/cycle with wide wheels and negative trail steering based on the python-lowracer bicycle. The design goals include adjustable dimensions to accommodate passengers up to 2 meters tall and weights up to 180 kg.

This project is intended as an educational endeavor to better learn about CAD, physics, engineering, and modeling physical systems.

**Directory Structure**

./cad
FreeCAD drawings, Python3 (programming language) scripts, Qt UI files.

./doc
Project documentation.

**Tested On**

```
OS: Ubuntu 19.10
Word size of OS: 64-bit
Word size of FreeCAD: 64-bit
Version: 0.18.3.
Build type: Release
Python version: 3.7.4
Qt version: 5.12.4
Coin version: 4.0.0a
OCC version: 7.3.0
Locale: English/United States (en_US)
```

**Usage**

On the command line:

```
#  $ cd /path/to/chonkscooter/cad/
#  $ freecad simulation.py &
```

FreeCAD should open and then load the simulation GUI. As of the pre-alpha release, the only controls are front and rear swingarm angle.
